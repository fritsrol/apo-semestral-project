#include "str_utils.h"

int char2frame(char c, int yrow, int xcolumn, uint16_t forecolor, int size) {
	int cix = c - ' ';
	if (cix < 0) cix = 0;
	int w = font_winFreeSystem14x16.width[cix];
	int y, x, i, j, word_byte_line, word_width = w * size;
    // saves each line of the character
	for(word_byte_line = 0; word_byte_line < MASK_ROWS; word_byte_line++) {
        // gets a mask of the given line of the character
		int mask = font_winFreeSystem14x16.bits[cix * MASK_ROWS + word_byte_line];
		y = size * word_byte_line;
		for(int xi = 0; xi < word_width; xi++) {
			x = xi * size;
			if (mask & 0x8000) {
                // given pixel prints in square with side of the given size
				for (i = 0; i < size; ++i) {
					for (j = 0; j < size; ++j) {
						pixels[yrow + y + i][xcolumn + x + j] = forecolor;
					}
				}
			}
			mask = mask << 1;
		}
	}
	return word_width;
}

int str2frame(char* str, int yrow, int xcolumn, uint16_t forecolor, int size) {
	int w = 0; 
	char c;
	while((c = *str++) != 0){
		w += char2frame(c, yrow, xcolumn + w, forecolor, size);
	}
	return w;	
}

Point find_string_start(char *str, int str_font_size, int y_section_start, int x_section_start, int section_height, int section_width) {
    // compute the width of the text
    int str_width = 0;
	char c;
    while((c = *str++) != 0){
	    int cix = c - ' ';
	    if(cix < 0) cix = 0;
		str_width += font_winFreeSystem14x16.width[cix] * str_font_size;
	}
    // compute the upper left corner of the text
    Point ret;
    ret.x = x_section_start + (section_width - str_width) / 2;
    ret.y = y_section_start + (section_height - (MASK_ROWS * str_font_size)) / 2;
    return ret;
}
