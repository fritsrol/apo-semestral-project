#include "network.h"

void init_network() {
	my_ip = get_my_ip();
	printf("my_ip: %u\n", my_ip);
    pthread_create(&thread_id_slave_listen, NULL, slave_listen, NULL); 
}

void kill_network() {
	pthread_cancel(thread_id_slave_listen);
}

uint32_t get_my_ip() {
	char hostbuffer[256];
    int hostname; 
  
    hostname = gethostname(hostbuffer, sizeof(hostbuffer)); 
    if (hostname == -1) { 
		printf("Error while getting the hostname, my_ip set to 0\n");
        return 0; 
    } 

	char byte[] = {0, 0, 0, 0};
	char c;
	int i = 0;
	int b = 0;
	while ((c = hostbuffer[i]) != 0) {
		// skips three characters of "ip-"
		if (i < 3) {
			++i;
			continue;
		}
		if (c != '-') {
			byte[b] = byte[b] * 10 + (c - 48);
		}
		else {
			++b;
		}
		++i;
	}
	char *cp = byte;

	return *(uint32_t*)cp;
}

void *slave_listen() {
	int oldType;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldType);
	int sockfd;
	unsigned int len;
    struct sockaddr_in slave_addr, master_addr;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		printf("Error while creating a slave socket file descriptor, box does not listen to network commands!\n");
        return; 
    } 
      
    memset(&slave_addr, 0, sizeof(slave_addr)); 
    memset(&master_addr, 0, sizeof(master_addr)); 
      
    slave_addr.sin_family    = AF_INET;
    slave_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
    slave_addr.sin_port = htons(SLAVE_PORT); 

    if (bind(sockfd, (const struct sockaddr *)&slave_addr, sizeof(slave_addr)) < 0 ) { 
		printf("Error while initializing a slave socket file descriptor, box does not listen to network commands!\n");
        return; 
    } 

    char buffer[sizeof(Send_data)]; 

	while (true) {
		printf("Slave ceka na socket\n");
		printf("Slave waits for a socket\n");
		int data_len = recvfrom(sockfd, (char *)buffer, sizeof(buffer), MSG_WAITALL, (struct sockaddr *)&master_addr, &len);
		if (master_addr.sin_addr.s_addr == 0) {
		}
		else if (strncmp(buffer, BROADCAST_COMMAND, COMMAND_LENGTH) == 0) {
			printf("Received BROADCAST_COMMAND from %u\n", master_addr.sin_addr.s_addr);
			if (master_addr.sin_addr.s_addr == my_ip) {
				printf("It was my own broadcast command\n");
				continue;
			}
			char to_send[COMMAND_LENGTH] = SLAVE_COMMAND;
    		master_addr.sin_port = htons(MASTER_PORT); 
			sendto(sockfd, (const char *)&to_send, sizeof(to_send), 0, (const struct sockaddr *)&master_addr, sizeof(master_addr));
		}
		else if (data_len == sizeof(Send_data)) {
			Send_data *rec_data = (Send_data *)buffer;
			if (rec_data->command == STRUCT_COMMAND) {
				printf("Received a data structure\n");
				pthread_mutex_lock(&flashing_both_lock);
				flashing_both = rec_data->flashing_both;
				pthread_mutex_unlock(&flashing_both_lock);
				pthread_mutex_lock(&flashing_left_lock);
    			flashing_left = rec_data->flashing_left;
				pthread_mutex_unlock(&flashing_left_lock);
				pthread_mutex_lock(&flashing_right_lock);
    			flashing_right = rec_data->flashing_right;
				pthread_mutex_unlock(&flashing_right_lock);
				pthread_mutex_lock(&left_diode_lock);
    			left_diode = rec_data->left_diode;
				pthread_mutex_unlock(&left_diode_lock);
				pthread_mutex_lock(&left_diode_changed_lock);
    			left_diode_changed = rec_data->left_diode_changed;
				pthread_mutex_unlock(&left_diode_changed_lock);
				pthread_mutex_lock(&right_diode_lock);
    			right_diode = rec_data->right_diode;
				pthread_mutex_unlock(&right_diode_lock);
				pthread_mutex_lock(&right_diode_changed_lock);
    			right_diode_changed = rec_data->right_diode_changed;
				pthread_mutex_unlock(&right_diode_changed_lock);
				pthread_mutex_lock(&sliding_lock);
    			sliding = rec_data->sliding;
				pthread_mutex_unlock(&sliding_lock);
			}
			else {
				printf("Received something else\n");
			}
		}
		else {
			printf("Received something else\n");
		}
	}
}

void *master_listen(void *args_void) {
	Master_listen_args *args = (Master_listen_args *)args_void;
	int sockfd;
	unsigned int len;
    struct sockaddr_in slave_addr, master_addr;

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        printf("Chyba pri vytvareni master socket file descriptor, box nemuze vydavat network prikazy!\n");
		printf("Error while creating a master socket file descriptor, box cannot send network commands!\n");
        return; 
    } 
      
    memset(&slave_addr, 0, sizeof(slave_addr)); 
    memset(&master_addr, 0, sizeof(master_addr)); 
      
    master_addr.sin_family = AF_INET;
    master_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
    master_addr.sin_port = htons(MASTER_PORT); 

    if (bind(sockfd, (const struct sockaddr *)&master_addr, sizeof(master_addr)) < 0 ) { 
        return; 
    }

    char buffer[sizeof(Send_data)]; 

	while (true) {
		printf("Master waits for a socket\n");
		recvfrom(sockfd, (char *)buffer, sizeof(buffer), MSG_WAITALL, (struct sockaddr *)&slave_addr, &len);
		if (strncmp(buffer, SLAVE_COMMAND, COMMAND_LENGTH) == 0 && slave_addr.sin_addr.s_addr != 0) {
			printf("Received SLAVE_COMMAND\n");
			if (slave_addr.sin_addr.s_addr == my_ip) {
				printf("It was my own slave command\n");
				continue;
			}
			bool skip = false;	// does not save one IP more than once
			for (int i = 0; i < args->found_listeners->count; ++i) {
				if (args->found_listeners->listeners_addr[i].sin_addr.s_addr == slave_addr.sin_addr.s_addr) {
					printf("IP already saved\n");
					skip = true;
					break;
				}
			}
			if (skip) {
				continue;
			}
			if (args->found_listeners->count == args->found_listeners->allocated_size) {
				args->found_listeners->allocated_size += 10;
				args->found_listeners->listeners_addr = (struct sockaddr_in*)realloc(args->found_listeners->listeners_addr, sizeof(struct sockaddr_in) * args->found_listeners->allocated_size);
			}
			args->found_listeners->listeners_addr[args->found_listeners->count] = slave_addr;
			++(args->found_listeners->count);
			printf("Added IP: %u\n", slave_addr.sin_addr.s_addr);
			printf("IP added in total: %d\n", args->found_listeners->count);
			*(args->repaint) = true;
		}
	}
}

void send_broadcast() {
  	int sockfd;
  	struct sockaddr_in broadcast_addr;
    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		printf("Error while sending BROADCAST_COMMAND, BROADCAST_COMMAND was not sent!\n");
        return;
    }

    int broadcast = 1;
	if (setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, & broadcast, sizeof broadcast) == -1) {
		printf("Error while sending BROADCAST_COMMAND, BROADCAST_COMMAND was not sent!\n");
        return;
    }

  	memset(&broadcast_addr, 0, sizeof(broadcast_addr));  		
  	broadcast_addr.sin_family = AF_INET; 						
  	broadcast_addr.sin_addr.s_addr = inet_addr(BROADCAST_IP); 	
  	broadcast_addr.sin_port = htons(SLAVE_PORT); 				

	char to_send[COMMAND_LENGTH] = BROADCAST_COMMAND;
	
	int tries = 3;
	for (int i = 0; i < tries; ++i) {
    	sendto(sockfd, (const char *)&to_send, sizeof(to_send), 0, (const struct sockaddr *)&broadcast_addr, sizeof(broadcast_addr));
	}
	printf("BROADCAST_COMMAND sent %dtimes\n", tries);
    close(sockfd);
}

void send_control(struct sockaddr_in servaddr) {
    int sockfd; 

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		printf("Error while sending a data structure, the data structure was not sent!\n");
        return;
    }

    Send_data to_send;
	to_send.command = STRUCT_COMMAND;
	pthread_mutex_lock(&flashing_both_lock);
    to_send.flashing_both = flashing_both;
	pthread_mutex_unlock(&flashing_both_lock);
	pthread_mutex_lock(&flashing_left_lock);
    to_send.flashing_left = flashing_left;
	pthread_mutex_unlock(&flashing_left_lock);
	pthread_mutex_lock(&flashing_right_lock);
    to_send.flashing_right = flashing_right;
	pthread_mutex_unlock(&flashing_right_lock);
	pthread_mutex_lock(&left_diode_lock);
    to_send.left_diode = left_diode;
	pthread_mutex_unlock(&left_diode_lock);
	pthread_mutex_lock(&left_diode_changed_lock);
    to_send.left_diode_changed = left_diode_changed;
	pthread_mutex_unlock(&left_diode_changed_lock);
	pthread_mutex_lock(&right_diode_lock);
    to_send.right_diode = right_diode;
	pthread_mutex_unlock(&right_diode_lock);
	pthread_mutex_lock(&right_diode_changed_lock);
    to_send.right_diode_changed = right_diode_changed;
	pthread_mutex_unlock(&right_diode_changed_lock);	
	pthread_mutex_lock(&sliding_lock);
    to_send.sliding = sliding;
	pthread_mutex_unlock(&sliding_lock);

    sendto(sockfd, (const char *)&to_send, sizeof(to_send), 0, (const struct sockaddr *)&servaddr, sizeof(servaddr));
    close(sockfd);
	printf("A data structure sent\n");
}

