#ifndef _MEM_BASE_H_
#define _MEM_BASE_H_

#include <stdint.h>
#include "mzapo_regs.h"
#include "mzapo_phys.h"

/* -------------------------------------- GLOBAL VARIABLES ------------------------------------------ */

uint8_t *mem_base;

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Inicializuje hardwarovou adresu mem_base.
 * Initialize hardware address mem_base.
 */
void init_mem_base();

#endif // _MEM_BASE_H_
