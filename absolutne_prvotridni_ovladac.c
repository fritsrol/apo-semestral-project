#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>

#include "lcd.h"
#include "diodes.h"
#include "animation.h"
#include "menu_main.h"
#include "global_settings.h"
#include "knobs.h"
#include "mem_base.h"
#include "network.h"

/* ------------------------------------- ALL GLOBAL VARIABLES ----------------------------------------- */

extern uint8_t *mem_base;
// current value of all knobs as unsigned int
extern uint32_t *knobs;
// current value of each knob's spinning and knob's pressing
extern uint8_t *knob_click;
extern uint8_t *knob_red;
extern uint8_t *knob_green;
extern uint8_t *knob_blue; 
// matrix of pixels to be shown in display
extern uint16_t pixels[LCD_HEIGHT][LCD_WIDTH];
// diodes' colors
extern Color left_diode;           
extern Color left_diode_changed;   
extern Color right_diode;          
extern Color right_diode_changed;  
// sliding change of diodes
extern volatile Diodes_bool sliding;
// diodes' flashing
extern volatile Flashing flashing_left;
extern volatile Flashing flashing_right;
extern volatile Flashing flashing_both;
// language: true = english, false = czech
extern bool english;
// knobs' sensibility for colors' HSV values changing
extern int knobs_sensibility;
// texts' size
extern int font_size;
// local IP address
extern uint32_t my_ip;

/* ------------------------------------- FUNCTION DECLARATION ----------------------------------------- */

/*
 * Inicializuje vse potrebne po spusteni.
 * Initialize all needed values after turning on.
 */
void init_all();

/*
 * Ukonci vse potrebne pred vypnutim.
 * Finishes all running processes.
 */
void kill_all();

/* ------------------------------------- MAIN FUNCTION ----------------------------------------- */

int main(int argc, char *argv[]) {
    init_all();
    start_screen();
    menu_main();
    quit_screen(english);
    kill_all();
    return 0;
}

/* ------------------------------------- FUNCTION DEFINITION ----------------------------------------- */

void init_all() {
	// HW addresses
	init_mem_base();
	init_knobs();
	init_lcd();
	// diodes' colors and threads
	init_diodes();
	// global adjustable variables
	init_global_settings();
	// network's threads
	init_network();
}

void kill_all() {
	kill_diodes();
	kill_network();
}
