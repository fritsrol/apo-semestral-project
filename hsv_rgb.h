#ifndef HSV_RGB
#define HSV_RGB
#include <math.h>
#include <assert.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct RGB
{
	unsigned char R;
	unsigned char G;
	unsigned char B;
}RGB888;

typedef struct HSV
{
	double H;
	double S;
	double V;
}HSV;

HSV RGBToHSV(RGB888 rgb);

RGB888 HSVToRGB(HSV hsv);

unsigned int recreateRGB(RGB888 color); // bylo unsigned long

unsigned long createRGBfromHSV(HSV color);

uint16_t our888to565(unsigned int color);

unsigned int our565to888(uint16_t color);

uint16_t rgb888torgb565m(RGB888 rgb888Pixel);

uint16_t hsvtorgb565old(HSV hsv);

uint16_t hsvtorgb565(double h, double s, double v);

unsigned int hsvtorgb888(double h, double s, double v);

#ifdef __cplusplus
} /* extern "C"*/
#endif
#endif
