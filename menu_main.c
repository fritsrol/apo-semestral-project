#include "menu_main.h"

void menu_main() {
    int shown_column = 0;    // 0 = left diode, 1 = both diodes, 2 = right diode
    int shown_row = 1;       // 1 = instant change, 2 = sudden change, 3 = slide change, 4 = copy/switch colors, 5 = flashing on/off, 6 = flash time, 7 = flash gab, 8 = phase shift
    int selected = 0;        // 0 = column change, 1 = row change, 2 = choice control
    uint8_t last_valid_red_value = *knob_red / 4;
    uint8_t last_valid_green_value = *knob_green / 4;
    uint8_t last_valid_blue_value = *knob_blue / 4;
    // original diplay set up
    show_menu_main(shown_column, shown_row, selected);
    while (true) {
        bool repaint = false;
        // pressing red knob - show settings menu
        if (*knob_click == 4) {
			while (*knob_click == 4) {
				printf(".");
			}
            menu_settings();
            repaint = true;
        }
        // pressing green knob - going deeper in controling
        else if (*knob_click == 2) {
			while (*knob_click == 2) {
				printf(".");
			}
            // only 0-2
            if (selected < 2) {
                ++selected;
            }
            else if (selected == 2) {
                // sudden change confirm
                if (shown_row == 2) {
                    if (shown_column == 0) {
                        set_left_diode();
                    }
                    else if (shown_column == 1) {
                        set_left_diode();
                        set_right_diode();
                    }
                    else /*if (shown_column == 0)*/ {
                        set_right_diode();
                    }
                }
                // sliding turning on/off
                else if (shown_row == 3) {
					pthread_mutex_lock(&sliding_lock);
                    if (shown_column == 0) { 
						sliding.left = !sliding.left;
						sliding.both = false;
                    }
                    else if (shown_column == 1) {
						sliding.both = !sliding.both;
						sliding.left = false;
						sliding.right = false;
                    }
                    else /*if (shown_column == 0)*/ { 
						sliding.right = !sliding.right;
						sliding.both = false;
                    }
					pthread_mutex_unlock(&sliding_lock);
                }
                // copying/switching diodes' colors
                else if (shown_row == 4) {
                    if (shown_column == 0) {
                    	copy_from_right_diode_to_left();
						set_left_diode();
                    }
                    else if (shown_column == 1) {
						switch_diodes();
						set_left_diode();
						set_right_diode();
                    }
                    else /*if (shown_column == 0)*/ {
                    	copy_from_left_diode_to_right();
						set_right_diode();
                    }
                }
                // flashing turning on/off
                else if (shown_row == 5) {
					pthread_mutex_lock(&flashing_left_lock);
					pthread_mutex_lock(&flashing_right_lock);
					pthread_mutex_lock(&flashing_both_lock);
                    if (shown_column == 0) {
                        flashing_left.flashing_on = !flashing_left.flashing_on;
                        flashing_both.flashing_on = false;
                    }
                    else if (shown_column == 1) {
                        flashing_both.flashing_on = !flashing_both.flashing_on;
                        flashing_left.flashing_on = false;
                        flashing_right.flashing_on = false;
                    }
                    else /*if (shown_column == 0)*/ {
                        flashing_right.flashing_on = !flashing_right.flashing_on;
                        flashing_both.flashing_on = false;
                    }
					pthread_mutex_unlock(&flashing_left_lock);
					pthread_mutex_unlock(&flashing_right_lock);
					pthread_mutex_unlock(&flashing_both_lock);
                }
            }
            repaint = true;
        }
		// pressing blue knob - back/exit
        else if (*knob_click == 1) {
			while (*knob_click == 1) {
				printf(".");
			}
            // exiting the program
            if (selected == 0) {
				pthread_mutex_lock(&sliding_lock);
				sliding.left = false;
				sliding.right = false;
				sliding.both = false;
				pthread_mutex_unlock(&sliding_lock);
				// zhasne displej
				set_background_color(BLACK);
				repaint_lcd();
				// zhasne diody
				pthread_mutex_lock(&left_diode_lock);
				left_diode.rgb565 = BLACK;
				*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = our565to888(BLACK);
				pthread_mutex_unlock(&left_diode_lock);
				pthread_mutex_lock(&right_diode_lock);
				right_diode.rgb565 = BLACK;
				*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = our565to888(BLACK);
				pthread_mutex_unlock(&right_diode_lock);
                return;
            }
            else {
                --selected;
                repaint = true;
            }
        }
        uint8_t valid_red_value = *knob_red / 4;
        uint8_t valid_green_value = *knob_green / 4;
        uint8_t valid_blue_value = *knob_blue / 4;
        // computes current knobs' spin change value
        double red_change = 0;
        double green_change = 0;
        double blue_change = 0;
		if (last_valid_red_value != valid_red_value || last_valid_green_value != valid_green_value || last_valid_blue_value != valid_blue_value) {
            // spin of the red knob in the positive way
            if ((valid_red_value > last_valid_red_value) || (last_valid_red_value >= 62 && valid_red_value < 32)) {
                red_change = 1.0;
            }
			// spin of the red knob in the negative way
            else if ((valid_red_value < last_valid_red_value) || (last_valid_red_value <= 1 && valid_red_value > 32)) {
                red_change = -1.0;
            }
			// spin of the green knob in the positive way
            if ((valid_green_value > last_valid_green_value) || (last_valid_green_value >= 62 && valid_green_value < 32)) {
                green_change = 1.0;
            }
			// spin of the green knob in the negative way
            else if ((valid_green_value < last_valid_green_value) || (last_valid_green_value <= 1 && valid_green_value > 32)) {
                green_change = -1.0;
            }
			// spin of the blue knob in the positive way
            if ((valid_blue_value > last_valid_blue_value) || (last_valid_blue_value >= 62 && valid_blue_value < 32)) {
                blue_change = 1.0;
            }
			// spin of the blue knob in the negative way
            else if ((valid_blue_value < last_valid_blue_value) || (last_valid_blue_value <= 1 && valid_blue_value > 32)) {
                blue_change = -1.0;
            }
            last_valid_red_value = valid_red_value;
            last_valid_green_value = valid_green_value;
            last_valid_blue_value = valid_blue_value;
		}
        // column change - green knob's change is important
        if (selected == 0 && green_change != 0) {
            shown_column += (int)green_change;
            if (shown_column == -1) {
                shown_column = 2;
            }
            else if (shown_column == 3) {
                shown_column = 0;
            }
            if (shown_row > 7) {    // middle menu (for both diodes) has one more option
                shown_row = 7;
            }
            repaint = true;
        }
		// row change - green knob's change is important
        else if (selected == 1 && green_change != 0) {
            shown_row += (int)green_change;
            if (shown_row < 1) {
                shown_row = 7;
            }
            else if (shown_row > 8) {   // prostredni menu ma o moznost vic
                shown_row = 1;  
            }
            else if (shown_row > 7 && shown_column != 1) {
                shown_row = 1;
            }
            repaint = true;
        }
		// control change - all knobs' changes are important
        else if (selected == 2 && (red_change != 0 || green_change != 0 || blue_change != 0)) {
            // LEFT DIODE
            if (shown_column == 0) {
                // instant change = converts HSV change and shows it immediately
                if (shown_row == 1) {
                    change_left_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                    set_left_diode();
                }
                // sudden change = converts HSV change only, shows it after pressing the green knob
                else if (shown_row == 2) {
                    change_left_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                }
                // slide change = converts HSV change only, shows it as a sliding after pressing the green knob
                else if (shown_row == 3) {
                    change_left_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                }
                // 4 (copying of diode's color) and 5 (flashing turning on/off) is controled by green knob's pressing
                // flashing time change
                else if (shown_row == 6) {
					pthread_mutex_lock(&flashing_left_lock);
                    flashing_left.flashing_time += (int)green_change;
                    if (flashing_left.flashing_time < 0) {
                        flashing_left.flashing_time = 0;
                    }
                    if (flashing_left.flashing_time > 9) {
                        flashing_left.flashing_time = 9;
                    }
					pthread_mutex_unlock(&flashing_left_lock);
                }
				// flashing gab change
                else if (shown_row == 7) {
					pthread_mutex_lock(&flashing_left_lock);
                    flashing_left.flashing_gab += (int)green_change;
                    if (flashing_left.flashing_gab < 0) {
                        flashing_left.flashing_gab = 0;
                    }
                    if (flashing_left.flashing_gab > 9) {
                        flashing_left.flashing_gab = 9;
                    }
					pthread_mutex_unlock(&flashing_left_lock);
                }
            }
            // BOTH DIODES
            else if (shown_column == 1) {
				// instant change = converts HSV change and shows it immediately
                if (shown_row == 1) {
                    change_left_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                    set_left_diode();
                    change_right_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                    set_right_diode();
                }
				// sudden change = converts HSV change only, shows it after pressing the green knob
                else if (shown_row == 2) {
                    change_left_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                    change_right_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                }
				// slide change = converts HSV change only, shows it as a sliding after pressing the green knob
                else if (shown_row == 3) {
                    change_left_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                    change_right_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                }
				// 4 (switching diodes colors) and 5 (flashing turning on/off) is controled by green knob's pressing
                // flashing time change
                else if (shown_row == 6) {
					pthread_mutex_lock(&flashing_both_lock);
                    flashing_both.flashing_time += (int)green_change;
                    if (flashing_both.flashing_time < 1) {
                        flashing_both.flashing_time = 1;
                    }
                    if (flashing_both.flashing_time > 9) {
                        flashing_both.flashing_time = 9;
                    }
					pthread_mutex_unlock(&flashing_both_lock);
                }
				// flashing gab change
                else if (shown_row == 7) {
					pthread_mutex_lock(&flashing_both_lock);
                    flashing_both.flashing_gab += (int)green_change;
                    if (flashing_both.flashing_gab < 1) {
                        flashing_both.flashing_gab = 1;
                    }
                    if (flashing_both.flashing_gab > 9) {
                        flashing_both.flashing_gab = 9;
                    }
					pthread_mutex_unlock(&flashing_both_lock);
                }
				// flashing phase shift change
                else if (shown_row == 8) {
					pthread_mutex_lock(&flashing_both_lock);
                    flashing_both.phase_shift += (int)green_change;
                    if (flashing_both.phase_shift < -9) {
                        flashing_both.phase_shift = -9;
                    }
                    if (flashing_both.phase_shift > 9) {
                        flashing_both.phase_shift = 9;
                    }
					pthread_mutex_unlock(&flashing_both_lock);
                }
            }
            // RIGHT DIODE
            else /*if (shown_column == 2)*/ {
				// instant change = converts HSV change and shows it immediately
                if (shown_row == 1) {
                    change_right_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                    set_right_diode();
                }
				// sudden change = converts HSV change only, shows it after pressing the green knob
                else if (shown_row == 2) {
                    change_right_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                }
				// slide change = converts HSV change only, shows it as a sliding after pressing the green knob
                else if (shown_row == 3) {
                    change_right_diode_hsv(red_change * 5 * knobs_sensibility, green_change * 0.05 * knobs_sensibility, blue_change * 0.05 * knobs_sensibility);
                }
				// 4 (copying of diode's color) and 5 (flashing turning on/off) is controled by green knob's pressing
                // flashing time change
                else if (shown_row == 6) {
					pthread_mutex_lock(&flashing_right_lock);
                    flashing_right.flashing_time += (int)green_change;
                    if (flashing_right.flashing_time < 1) {
                        flashing_right.flashing_time = 1;
                    }
                    if (flashing_right.flashing_time > 9) {
                        flashing_right.flashing_time = 9;
                    }
					pthread_mutex_unlock(&flashing_right_lock);
                }
				// flashing gab change
                else if (shown_row == 7) {
					pthread_mutex_lock(&flashing_right_lock);
                    flashing_right.flashing_gab += (int)green_change;
                    if (flashing_right.flashing_gab < 1) {
                        flashing_right.flashing_gab = 1;
                    }
                    if (flashing_right.flashing_gab > 9) {
                        flashing_right.flashing_gab = 9;
                    }
					pthread_mutex_unlock(&flashing_right_lock);
                }
            }
            repaint = true;
        }
        if (repaint) {
            show_menu_main(shown_column, shown_row, selected);
        }
        else if (sliding.left || sliding.right || sliding.both) {
            set_rect_color(left_diode.rgb565, 			100, 	0,		 			200, 	100);
            set_rect_color(right_diode.rgb565, 	100, 	LCD_WIDTH - 100, 	200, 	LCD_WIDTH);
            repaint_lcd();
        }
    }
}

void show_menu_main(int shown_column, int shown_row, int selected) {
    // menu background and diodes' colors shown in display
    set_rect_color(WHITE, 0, 100, 200, LCD_WIDTH - 100);
    set_rect_color(left_diode_changed.rgb565, 	0, 		0, 					100, 	100);
    set_rect_color(right_diode_changed.rgb565, 	0, 		LCD_WIDTH - 100, 	100, 	LCD_WIDTH);
    set_rect_color(left_diode.rgb565, 			100, 	0,		 			200, 	100);
    set_rect_color(right_diode.rgb565, 			100, 	LCD_WIDTH - 100, 	200, 	LCD_WIDTH);
    set_rect_color(WHITE, 200, 0, LCD_HEIGHT, LCD_WIDTH);
	// current menu's column's texts
	show_menu_main_text(shown_column, shown_row, selected);
    // knobs' functions description
    char *settings = english ? "Settings\0" : "Nastaveni\0";
    Point point = find_string_start(settings, font_size, LCD_HEIGHT - MASK_ROWS * font_size, 0, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    str2frame(settings, point.y, point.x, BLACK, font_size);
    char *choose = english ? "Choose\0" : "Vybrat\0";
    point = find_string_start(choose, font_size, LCD_HEIGHT - MASK_ROWS * font_size, LCD_WIDTH_THIRD, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    str2frame(choose, point.y, point.x, BLACK, font_size);
    char *back_eng = selected == 0 ? "Exit\0" : "Back\0";
    char *back_cze = selected == 0 ? "Konec\0" : "Zpet\0";
    char *back = english ? back_eng : back_cze;
    point = find_string_start(back, font_size, LCD_HEIGHT - MASK_ROWS * font_size, LCD_WIDTH_TWO_THIRDS, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    str2frame(back, point.y, point.x, BLACK, font_size);
    // repaints display
    repaint_lcd();
}

void show_menu_main_text(int shown_column, int shown_row, int selected) {
    // rows are mostly black
	uint16_t colors[9];
	for (int i = 0; i < 9; ++i) {
		colors[i] = BLACK;
	}
    // first row is red when the user is changing columns
	if (selected == 0) {
		colors[0] = RED;
	}
    // one another row is colorful when the user is changing row or controling the feature
	else {
    	colors[shown_row] = selected == 2 ? RED : BLUE;
	}
    int row_height = (MASK_ROWS * font_size);
    char *txt_eng = "\0";
    char *txt_cze = "\0";
    char *txt;
	Point point;
    // row 1
	if (shown_column == 0) {
    	txt_eng = "Left diode\0";
    	txt_cze = "Leva dioda\0";
	}
	else if (shown_column == 1) {
    	txt_eng = "Both diodes\0";
    	txt_cze = "Obe diody\0";
	}
	else {
    	txt_eng = "Right diode\0";
    	txt_cze = "Prava dioda\0";
	}
    txt = english ? txt_eng : txt_cze;
	point = find_string_start(txt, font_size, 0 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[0], font_size);
	// row 2
    txt_eng = "Instant change\0";
    txt_cze = "Okamzita zmena\0";
    txt = english ? txt_eng : txt_cze;
	point = find_string_start(txt, font_size, 1 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[1], font_size);
	// row 3
    txt_eng = "Sudden change\0";
    txt_cze = "Skokova zmena\0";
    txt = english ? txt_eng : txt_cze;
	point = find_string_start(txt, font_size, 2 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[2], font_size);
    // row 4
    txt_eng = "Slide change\0";
    txt_cze = "Postupna zmena\0";
    txt = english ? txt_eng : txt_cze;
	point = find_string_start(txt, font_size, 3 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[3], font_size);
    // row 5
	if (shown_column == 0) {
    	txt_eng = "Copy from right\0";
    	txt_cze = "Zkopirovat pravou\0";
	}
	else if (shown_column == 1) {
    	txt_eng = "Switch colors\0";
    	txt_cze = "Prohodit barvy\0";
	}
	else {
    	txt_eng = "Copy from left\0";
    	txt_cze = "Zkopirovat levou\0";
	}
    txt = english ? txt_eng : txt_cze;
	point = find_string_start(txt, font_size, 4 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[4], font_size);
    // row 6
	bool cur_flashing = false;
	if (shown_column == 0) {
    	cur_flashing = flashing_left.flashing_on;
	}
	else if (shown_column == 1) {
    	cur_flashing = flashing_both.flashing_on;
	}
	else {
    	cur_flashing = flashing_right.flashing_on;
	}
    txt_eng = cur_flashing ? "Turn off flashing\0" : "Turn on flashing\0";
    txt_cze = cur_flashing ? "Vypnout blikani\0" : "Zapnout blikani\0";
    txt = english ? txt_eng : txt_cze;
	point = find_string_start(txt, font_size, 5 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[5], font_size);
    // row 7
	int cur_flashing_time = 0;
	if (shown_column == 0) {
    	cur_flashing_time = flashing_left.flashing_time;
	}
	else if (shown_column == 1) {
    	cur_flashing_time = flashing_both.flashing_time;
	}
	else {
    	cur_flashing_time = flashing_right.flashing_time;
	}
    char flash_txt_1_eng[] = {'F', 'l', 'a', 's', 'h', ' ', 't', 'i', 'm', 'e', ':', ' ', cur_flashing_time + 48, 0};
    char flash_txt_1_cze[] = {'C', 'a', 's', ' ', 'b', 'l', 'i', 'k', 'a', 'n', 'i', ':', ' ', cur_flashing_time + 48, 0};
    txt = english ? flash_txt_1_eng : flash_txt_1_cze;
	point = find_string_start(txt, font_size, 6 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[6], font_size);
    // row 8
	int cur_flashing_gab = 0;
	if (shown_column == 0) {
    	cur_flashing_gab = flashing_left.flashing_gab;
	}
	else if (shown_column == 1) {
    	cur_flashing_gab = flashing_both.flashing_gab;
	}
	else {
    	cur_flashing_gab = flashing_right.flashing_gab;
	}
    char flash_txt_2_eng[] = {'F', 'l', 'a', 's', 'h', ' ', 'g', 'a', 'b', ':', ' ', cur_flashing_gab + 48, 0};
    char flash_txt_2_cze[] = {'P', 'r', 'o', 'd', 'l', 'e', 'v', 'a',  ' ', 'b', 'l', 'i', 'k', 'a', 'n', 'i', ':', ' ', cur_flashing_gab + 48, 0};
    txt = english ? flash_txt_2_eng : flash_txt_2_cze;
	point = find_string_start(txt, font_size, 7 * row_height, 0, row_height, LCD_WIDTH);
    str2frame(txt, point.y, point.x, colors[7], font_size);
    // row 9 - only in the middle menu (both diodes phase shift)
    if (shown_column == 1) {
        char sign = '+';
        int abs_phase_shift = flashing_both.phase_shift;
        if (abs_phase_shift < 0) {
            sign = '-';
            abs_phase_shift *= -1;
        }
        char phase_shift_txt_eng[] = {'P', 'h', 'a', 's', 'e', ' ', 's', 'h', 'i', 'f', 't', ':', ' ', sign, abs_phase_shift + 48, 0};
        char phase_shift_txt_cze[] = {'F', 'a', 'z', 'o', 'v', 'y', ' ', 'p',  'o', 's', 'u', 'n', ':', ' ', sign, abs_phase_shift + 48, 0};
        txt = english ? phase_shift_txt_eng : phase_shift_txt_cze;
	    point = find_string_start(txt, font_size, 8 * row_height, 0, row_height, LCD_WIDTH);
        str2frame(txt, point.y, point.x, colors[8], font_size);
    }
}

