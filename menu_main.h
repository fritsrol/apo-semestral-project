#ifndef _MENU_MAIN_H_
#define _MENU_MAIN_H_

#include <stdio.h>
#include "hsv_rgb.h"
#include "str_utils.h" 
#include "diodes.h"
#include "global_settings.h"
#include "rgb565_colors.h"
#include "knobs.h"
#include "menu_settings.h" 

/* -------------------------------------- FUNCTION DECLARATAION ------------------------------------------ */

/*
 * Hlavni menu ovladajici diody.
 * Main menu controling the diodes.
 */
void menu_main();

/*
 * Zobrazi hlavni menu.
 * Shows the main menu.
 */
void show_menu_main(int shown_column, int shown_row, int selected);

/*
 * Zobrazi text v hlavnim menu podle zvoleneho jazyka a velikosti textu.
 * Shown main menu's texts. Uses global language and text size variables.
 */
void show_menu_main_text(int shown_column, int shown_row, int selected);

#endif // _MENU_MAIN_H_
