#include "menu_settings.h"

void menu_settings() {
    int shown_row = 0;       // 0 = language, 1 = font size, 2 = knobs sensibility, 3 = searching of network listeners
    int selected = 0;        // 0 = row change, 1 = setting control
    uint8_t last_valid_green_value = *knob_green / 4;
	// original diplay set up
    show_menu_settings(shown_row, selected);
    while (true) {
        bool repaint = false;
		// pressing green knob - going deeper in controling
        if (*knob_click == 2) {
			while (*knob_click == 2) {
				printf(".");
			}
            // shows network menu
            if (shown_row == 3) {
                menu_network();
                repaint = true;
            }
            // only 0-1
            else if (selected < 1) {
                ++selected;
                repaint = true;
            }
        }
		// pressing blue knob - back
        else if (*knob_click == 1) {
			while (*knob_click == 1) {
				printf(".");
			}
            if (selected == 0) {
                return;
            }
            else {
                --selected;
                repaint = true;
            }
        }
        uint8_t valid_green_value = *knob_green / 4;
		// computes current knobs' spin change value
        int green_change = 0;
		if (last_valid_green_value != valid_green_value) {
			// spin of the green knob in the positive way
                if ((valid_green_value > last_valid_green_value) || (last_valid_green_value >= 62 && valid_green_value < 32)) {
                    green_change = 1;
                }
				// spin of the green knob in the negative way
                else if ((valid_green_value < last_valid_green_value) || (last_valid_green_value <= 1 && valid_green_value > 32)) {
                    green_change = -1;
                }
                last_valid_green_value = valid_green_value;
		}
		// row change - green knob's change is important
        if (selected == 0 && green_change != 0) {
            shown_row += green_change;
            if (shown_row < 0) {
                shown_row = 3;
            }
            else if (shown_row > 3) {
                shown_row = 0;
            }
            repaint = true;
        }
		// control change - green knob's change is important
        else if (selected == 1 && green_change != 0) {
            // language
            if (shown_row == 0) {
				english = !english;
            }
            // font size
            else if (shown_row == 1) {
				if (font_size == 1) {
					font_size = 2;
				}
				else {
					font_size = 1;
				}
            }
            // knobs' sensibility
            else /*if (shown_row == 2)*/ {
				knobs_sensibility += green_change;
				if (knobs_sensibility < 1) {
					knobs_sensibility = 1;
				}
				else if (knobs_sensibility > 5) {
					knobs_sensibility = 5;
				}
            }
            repaint = true;
        }
        if (repaint) {
            show_menu_settings(shown_row, selected);
        }
    }
}

void show_menu_settings(int shown_row, int selected) {
    set_background_color(WHITE);
    // colors of shown rows
    uint16_t colors[4];
    colors[0] = BLACK; colors[1] = BLACK; colors[2] = BLACK; colors[3] = BLACK;
	colors[shown_row] = selected == 0 ? BLUE : RED;
    // language settings
    char *text = english ? "Language: English" : "Jazyk: Cestina";
    Point point = find_string_start(text, font_size, 0, 0, LCD_HEIGHT_FIFTH, LCD_WIDTH);
    str2frame(text, point.y, point.x, colors[0], font_size);
    // font size settings
    char text_font_eng[] = {'T', 'e', 'x', 't', 's', ' ', 's', 'i', 'z', 'e', ':', ' ', font_size + 48, 0};
    char text_font_cze[] = {'V', 'e', 'l', 'i', 'k', 'o', 's', 't', ' ', 't', 'e', 'x', 't', 'u', ':', ' ', font_size + 48, 0};
	text = english ? text_font_eng : text_font_cze;
    point = find_string_start(text, font_size, LCD_HEIGHT_FIFTH, 0, LCD_HEIGHT_FIFTH, LCD_WIDTH);
    str2frame(text, point.y, point.x, colors[1], font_size);
    // knobs' sensibility settings
    char text_knobs_eng[] = {'K', 'n', 'o', 'b', 's', ' ', 's', 'e', 'n', 's', 'i', 'b', 'i', 'l', 'i', 't', 'y', ':', ' ', knobs_sensibility + 48, 0};
    char text_knobs_cze[] = {'C', 'i', 't', 'l', 'i', 'v', 'o', 's', 't', ' ', 'k', 'n', 'o', 'b', 'u', ':', ' ', knobs_sensibility + 48, 0};
	text = english ? text_knobs_eng : text_knobs_cze;
    point = find_string_start(text, font_size, LCD_HEIGHT_TWO_FIFTHS, 0, LCD_HEIGHT_FIFTH, LCD_WIDTH);
    str2frame(text, point.y, point.x, colors[2], font_size);
    // network communication settings
    text = english ? "Find network listeners" : "Hledat sitove posluchace";
    point = find_string_start(text, font_size, LCD_HEIGHT_THREE_FIFTHS, 0, LCD_HEIGHT_FIFTH, LCD_WIDTH);
    str2frame(text, point.y, point.x, colors[3], font_size);
	// knobs' functions description
	if (selected == 0) {
    	text = english ? "Choose\0" : "Vybrat\0";
    	point = find_string_start(text, font_size, LCD_HEIGHT - MASK_ROWS * font_size, LCD_WIDTH_THIRD, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    	str2frame(text, point.y, point.x, BLACK, font_size);
	}
    text = english ? "Back\0" : "Zpet\0";
    point = find_string_start(text, font_size, LCD_HEIGHT - MASK_ROWS * font_size, LCD_WIDTH_TWO_THIRDS, MASK_ROWS * font_size, LCD_WIDTH_THIRD);
    str2frame(text, point.y, point.x, BLACK, font_size);
	// repaints display
    repaint_lcd();
}

