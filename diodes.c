#include "diodes.h"

void init_diodes() {
    left_diode_changed.hue = 0;
    left_diode_changed.saturation = 0.9;
    left_diode_changed.value = 0.9;
    compute_left_diode_color();
    set_left_diode();
    right_diode_changed.hue = 0;
    right_diode_changed.saturation = 0.9;
    right_diode_changed.value = 0.9;
    compute_right_diode_color();
	set_right_diode();

	flashing_left.flashing_on = false;
	flashing_left.flashing_time = 1;
	flashing_left.flashing_gab = 1;
	flashing_right.flashing_on = false;
	flashing_right.flashing_time = 1;
	flashing_right.flashing_gab = 1;
	flashing_both.flashing_on = false;
	flashing_both.flashing_time = 1;
	flashing_both.flashing_gab = 1;
    flashing_both.phase_shift = 0;

	sliding.left = false;
	sliding.right = false;
	sliding.both = false;

	if (pthread_mutex_init(&left_diode_lock, NULL) != 0) {
        printf("left_diode_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&left_diode_changed_lock, NULL) != 0) {
        printf("left_diode_changed_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&right_diode_lock, NULL) != 0) {
        printf("right_diode_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&right_diode_changed_lock, NULL) != 0) {
        printf("right_diode_changed_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&sliding_lock, NULL) != 0) {
        printf("sliding_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&flashing_left_lock, NULL) != 0) {
        printf("flashing_left_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&flashing_right_lock, NULL) != 0) {
        printf("flashing_right_lock mutex init failed\n");
    }
	if (pthread_mutex_init(&flashing_both_lock, NULL) != 0) {
        printf("flashing_both_lock mutex init failed\n");
    }
   
    pthread_create(&thread_id_left_diode, NULL, left_diode_light, NULL); 
    pthread_create(&thread_id_right_diode, NULL, right_diode_light, NULL); 
    pthread_create(&thread_id_left_slide, NULL, left_diode_slide, NULL); 
    pthread_create(&thread_id_right_slide, NULL, right_diode_slide, NULL); 
    pthread_create(&thread_id_both_slide, NULL, both_diodes_slide, NULL); 
}

void kill_diodes() {
	pthread_cancel(thread_id_left_diode);
	pthread_cancel(thread_id_right_diode);
	pthread_cancel(thread_id_left_slide);
	pthread_cancel(thread_id_right_slide);
	pthread_cancel(thread_id_both_slide);
	pthread_mutex_destroy(&left_diode_lock);
	pthread_mutex_destroy(&left_diode_changed_lock);
	pthread_mutex_destroy(&right_diode_lock);
	pthread_mutex_destroy(&right_diode_changed_lock);
	pthread_mutex_destroy(&sliding_lock);
	pthread_mutex_destroy(&flashing_left_lock);
	pthread_mutex_destroy(&flashing_right_lock);
	pthread_mutex_destroy(&flashing_both_lock);
}

void *left_diode_light() {
	int oldType;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldType);
	clock_t start;
	while (true) {
		pthread_mutex_lock(&left_diode_lock);
		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = our565to888(left_diode.rgb565);
		pthread_mutex_unlock(&left_diode_lock);
		// only left diode's flashing
		start = clock();
		bool blick = false;		// aby se nepreskocila zadna cast blikani
		while (flashing_left.flashing_on && flashing_left.flashing_time * 2 > (clock() - start) / CLOCKS_PER_SEC) {
			pthread_mutex_lock(&left_diode_lock);
			*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = our565to888(left_diode.rgb565);
			pthread_mutex_unlock(&left_diode_lock);
			blick = true;
		}
		start = clock();
		while (blick && flashing_left.flashing_on && flashing_left.flashing_gab * 2 > (clock() - start) / CLOCKS_PER_SEC) {
			*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0;
		}
		// both diodes' flashing
		start = clock();
		blick = false;		// aby se nepreskocila zadna cast blikani
        if (flashing_both.flashing_on) {
			blick = true;
        }
		// phase shift
        while (flashing_both.flashing_on && flashing_both.phase_shift * -2 > (clock() - start) / CLOCKS_PER_SEC) {
			*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0;
        }
        while (blick && flashing_both.flashing_on) {
		    start = clock();
            // shining
		    while (flashing_both.flashing_on && flashing_both.flashing_time * 2 > (clock() - start) / CLOCKS_PER_SEC) {
				pthread_mutex_lock(&left_diode_lock);
			    *(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = our565to888(left_diode.rgb565);
				pthread_mutex_unlock(&left_diode_lock);
		    }
		    start = clock();
            // not shining
		    while (flashing_both.flashing_on && flashing_both.flashing_gab * 2 > (clock() - start) / CLOCKS_PER_SEC) {
		    	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o) = 0;
		    }
        }
	}
}

void *right_diode_light() {
	int oldType;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldType);
	clock_t start;
	while (true) {
		pthread_mutex_lock(&right_diode_lock);
		*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = our565to888(right_diode.rgb565); 
		pthread_mutex_unlock(&right_diode_lock);
		// only right diode's flashing
		start = clock();
		bool blick = false;		// aby se nepreskocila zadna cast blikani
		while (flashing_right.flashing_on && flashing_right.flashing_time * 2 > (clock() - start) / CLOCKS_PER_SEC) {
			pthread_mutex_lock(&right_diode_lock);
			*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = our565to888(right_diode.rgb565);
			pthread_mutex_unlock(&right_diode_lock);
			blick = true;
		}
		start = clock();
		while (blick && flashing_right.flashing_on && flashing_right.flashing_gab * 2 > (clock() - start) / CLOCKS_PER_SEC) {
			*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0;
		}
		// both diodes' flashing
		start = clock();
		blick = false;		// aby se nepreskocila zadna cast blikani
        if (flashing_both.flashing_on) {
			blick = true;
        }
        // phase shift
        while (flashing_both.flashing_on && flashing_both.phase_shift * 2 > (clock() - start) / CLOCKS_PER_SEC) {
			*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0;
        }
        while (blick && flashing_both.flashing_on) {
		    start = clock();
			// shining
		    while (flashing_both.flashing_on && flashing_both.flashing_time * 2 > (clock() - start) / CLOCKS_PER_SEC) {
				pthread_mutex_lock(&right_diode_lock);
		    	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = our565to888(right_diode.rgb565);
				pthread_mutex_unlock(&right_diode_lock);
		    }
		    start = clock();
			// not shining
		    while (flashing_both.flashing_on && flashing_both.flashing_gab * 2 > (clock() - start) / CLOCKS_PER_SEC) {
		    	*(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o) = 0;
		    }
        }
	}
}

void *left_diode_slide() {
	int oldType;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldType);
	clock_t start;
	while (true) {
		pthread_mutex_lock(&left_diode_lock);
		pthread_mutex_lock(&left_diode_changed_lock);
		double hue_dif = (left_diode_changed.hue - left_diode.hue) / 100;
		double sat_dif = (left_diode_changed.saturation - left_diode.saturation) / 100;
		double value_dif = (left_diode_changed.value - left_diode.value) / 100;
		pthread_mutex_unlock(&left_diode_changed_lock);
		double start_hue = left_diode.hue;
		double start_sat = left_diode.saturation;
		double start_value = left_diode.value;
		pthread_mutex_unlock(&left_diode_lock);
		int cycle = 0;
		int increase = 1;
		while (sliding.left) {
			start = clock();
			while (clock() - start < CLOCKS_PER_SEC / 50) {
			}
			pthread_mutex_lock(&left_diode_lock);
			left_diode.rgb565 = hsvtorgb565(start_hue + cycle * hue_dif, start_sat + cycle * sat_dif, start_value + cycle * value_dif);
			pthread_mutex_unlock(&left_diode_lock);
			if (cycle == 100) {
				increase = -1;
			}
			else if (cycle == 0) {
				increase = 1;
			}
			cycle += increase;
		}
	}
}

void *right_diode_slide() {
	int oldType;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldType);
	clock_t start;
	while (true) {
		pthread_mutex_lock(&right_diode_lock);
		pthread_mutex_lock(&right_diode_changed_lock);
		double hue_dif = (right_diode_changed.hue - right_diode.hue) / 100;
		double sat_dif = (right_diode_changed.saturation - right_diode.saturation) / 100;
		double value_dif = (right_diode_changed.value - right_diode.value) / 100;
		pthread_mutex_unlock(&right_diode_changed_lock);
		double start_hue = right_diode.hue;
		double start_sat = right_diode.saturation;
		double start_value = right_diode.value;
		pthread_mutex_unlock(&right_diode_lock);
		int cycle = 0;
		int increase = 1;
		while (sliding.right) {
			start = clock();
			while (clock() - start < CLOCKS_PER_SEC / 50) {
			}
			pthread_mutex_lock(&right_diode_lock);
			right_diode.rgb565 = hsvtorgb565(start_hue + cycle * hue_dif, start_sat + cycle * sat_dif, start_value + cycle * value_dif);
			pthread_mutex_unlock(&right_diode_lock);
			if (cycle == 100) {
				increase = -1;
			}
			else if (cycle == 0) {
				increase = 1;
			}
			cycle += increase;
		}
	}
}

void *both_diodes_slide() {
	int oldType;
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldType);
	clock_t start;
	while (true) {
		double hue_dif_right;
		double sat_dif_right;
		double value_dif_right;
		double start_hue_right;
		double start_sat_right;
		double start_value_right;

		double hue_dif_left;
		double sat_dif_left;
		double value_dif_left;
		double start_hue_left;
		double start_sat_left;
		double start_value_left;

		bool save = true;
		int cycle = 0;
		int increase = 1;
		while (sliding.both) {
			if (save) {
				pthread_mutex_lock(&right_diode_lock);
				pthread_mutex_lock(&right_diode_changed_lock);
				hue_dif_right = (right_diode_changed.hue - right_diode.hue) / 100;
				sat_dif_right = (right_diode_changed.saturation - right_diode.saturation) / 100;
				value_dif_right = (right_diode_changed.value - right_diode.value) / 100;
				pthread_mutex_unlock(&right_diode_changed_lock);
				start_hue_right = right_diode.hue;
				start_sat_right = right_diode.saturation;
				start_value_right = right_diode.value;
				pthread_mutex_unlock(&right_diode_lock);

				pthread_mutex_lock(&left_diode_lock);
				pthread_mutex_lock(&left_diode_changed_lock);
				hue_dif_left = (left_diode_changed.hue - left_diode.hue) / 100;
				sat_dif_left = (left_diode_changed.saturation - left_diode.saturation) / 100;
				value_dif_left = (left_diode_changed.value - left_diode.value) / 100;
				pthread_mutex_unlock(&left_diode_changed_lock);
				start_hue_left = left_diode.hue;
				start_sat_left = left_diode.saturation;
				start_value_left = left_diode.value;
				pthread_mutex_unlock(&left_diode_lock);
				save = false;
			}
			start = clock();
			while (clock() - start < CLOCKS_PER_SEC / 50) {
			}
			pthread_mutex_lock(&right_diode_lock);
			right_diode.rgb565 = hsvtorgb565(start_hue_right + cycle * hue_dif_right, start_sat_right + cycle * sat_dif_right, start_value_right + cycle * value_dif_right);
			pthread_mutex_unlock(&right_diode_lock);
			pthread_mutex_lock(&left_diode_lock);
			left_diode.rgb565 = hsvtorgb565(start_hue_left + cycle * hue_dif_left, start_sat_left + cycle * sat_dif_left, start_value_left + cycle * value_dif_left);
			pthread_mutex_unlock(&left_diode_lock);
			if (cycle == 100) {
				increase = -1;
			}
			else if (cycle == 0) {
				increase = 1;
			}
			cycle += increase;
		}
	}
}

void compute_left_diode_color() {
	pthread_mutex_lock(&left_diode_changed_lock);
    left_diode_changed.rgb565 = hsvtorgb565(left_diode_changed.hue, left_diode_changed.saturation, left_diode_changed.value);
	pthread_mutex_unlock(&left_diode_changed_lock);
}

void compute_right_diode_color() {
	pthread_mutex_lock(&right_diode_changed_lock);
    right_diode_changed.rgb565 = hsvtorgb565(right_diode_changed.hue, right_diode_changed.saturation, right_diode_changed.value);
	pthread_mutex_unlock(&right_diode_changed_lock);
}

void set_left_diode() {
	pthread_mutex_lock(&left_diode_lock);
	pthread_mutex_lock(&left_diode_changed_lock);
	left_diode.hue = left_diode_changed.hue;
	left_diode.saturation = left_diode_changed.saturation;
	left_diode.value = left_diode_changed.value;
    left_diode.rgb565 = left_diode_changed.rgb565;
	pthread_mutex_unlock(&left_diode_changed_lock);
	pthread_mutex_unlock(&left_diode_lock);
}

void set_right_diode() {
	pthread_mutex_lock(&right_diode_lock);
	pthread_mutex_lock(&right_diode_changed_lock);
	right_diode.hue = right_diode_changed.hue;
	right_diode.saturation = right_diode_changed.saturation;
	right_diode.value = right_diode_changed.value;
    right_diode.rgb565 = right_diode_changed.rgb565;
	pthread_mutex_unlock(&right_diode_changed_lock);
	pthread_mutex_unlock(&right_diode_lock);
}

void copy_from_left_diode_to_right() {
	pthread_mutex_lock(&left_diode_changed_lock);
	pthread_mutex_lock(&right_diode_changed_lock);
    right_diode_changed.hue = left_diode_changed.hue;
    right_diode_changed.saturation = left_diode_changed.saturation;
    right_diode_changed.value = left_diode_changed.value;
    right_diode_changed.rgb565 = left_diode_changed.rgb565;
	pthread_mutex_unlock(&right_diode_changed_lock);
	pthread_mutex_unlock(&left_diode_changed_lock);
}

void copy_from_right_diode_to_left() {
	pthread_mutex_lock(&left_diode_changed_lock);
	pthread_mutex_lock(&right_diode_changed_lock);
    left_diode_changed.hue = right_diode_changed.hue;
    left_diode_changed.saturation = right_diode_changed.saturation;
    left_diode_changed.value = right_diode_changed.value;
    left_diode_changed.rgb565 = right_diode_changed.rgb565;
	pthread_mutex_unlock(&right_diode_changed_lock);
	pthread_mutex_unlock(&left_diode_changed_lock);
}

void switch_diodes() {
	pthread_mutex_lock(&left_diode_changed_lock);
	double hue = left_diode_changed.hue;
	double saturation = left_diode_changed.saturation;
	double value = left_diode_changed.value;
	uint16_t color = left_diode_changed.rgb565;
	pthread_mutex_unlock(&left_diode_changed_lock);

	copy_from_right_diode_to_left();

	pthread_mutex_lock(&right_diode_changed_lock);
	right_diode_changed.hue = hue;
    right_diode_changed.saturation = saturation;
    right_diode_changed.value = value;
    right_diode_changed.rgb565 = color;
	pthread_mutex_unlock(&right_diode_changed_lock);
}

void change_left_diode_hsv(int hue_change, double saturation_change, double value_change) {
	pthread_mutex_lock(&left_diode_changed_lock);
	left_diode_changed.hue += hue_change;
	if (left_diode_changed.hue < 0) {
		left_diode_changed.hue += 360;
	}
	else if (left_diode_changed.hue > 359) {
		left_diode_changed.hue -= 360;
	}
    left_diode_changed.saturation += saturation_change;
	if (left_diode_changed.saturation < 0) {
		left_diode_changed.saturation = 0;
	}
	else if (left_diode_changed.saturation > 1) {
		left_diode_changed.saturation = 1;
	}
    left_diode_changed.value += value_change;
	if (left_diode_changed.value < 0) {
		left_diode_changed.value = 0;
	}
	else if (left_diode_changed.value > 1) {
		left_diode_changed.value = 1;
	}
	pthread_mutex_unlock(&left_diode_changed_lock);
    compute_left_diode_color();
}

void change_right_diode_hsv(int hue_change, double saturation_change, double value_change) {
	pthread_mutex_lock(&right_diode_changed_lock);
	right_diode_changed.hue += hue_change;
	if (right_diode_changed.hue < 0) {
		right_diode_changed.hue += 360;
	}
	else if (right_diode_changed.hue > 359) {
		right_diode_changed.hue -= 360;
	}
    right_diode_changed.saturation += saturation_change;
	if (right_diode_changed.saturation < 0) {
		right_diode_changed.saturation = 0;
	}
	else if (right_diode_changed.saturation > 1) {
		right_diode_changed.saturation = 1;
	}
    right_diode_changed.value += value_change;
	if (right_diode_changed.value < 0) {
		right_diode_changed.value = 0;
	}
	else if (right_diode_changed.value > 1) {
		right_diode_changed.value = 1;
	}
	pthread_mutex_unlock(&right_diode_changed_lock);
    compute_right_diode_color();
}
